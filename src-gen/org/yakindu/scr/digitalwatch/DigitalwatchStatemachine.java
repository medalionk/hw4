package org.yakindu.scr.digitalwatch;
import org.yakindu.scr.ITimer;

public class DigitalwatchStatemachine implements IDigitalwatchStatemachine {

	protected class SCIButtonsImpl implements SCIButtons {
	
		private boolean topLeftPressed;
		
		public void raiseTopLeftPressed() {
			topLeftPressed = true;
		}
		
		private boolean topLeftReleased;
		
		public void raiseTopLeftReleased() {
			topLeftReleased = true;
		}
		
		private boolean topRightPressed;
		
		public void raiseTopRightPressed() {
			topRightPressed = true;
		}
		
		private boolean topRightReleased;
		
		public void raiseTopRightReleased() {
			topRightReleased = true;
		}
		
		private boolean bottomLeftPressed;
		
		public void raiseBottomLeftPressed() {
			bottomLeftPressed = true;
		}
		
		private boolean bottomLeftReleased;
		
		public void raiseBottomLeftReleased() {
			bottomLeftReleased = true;
		}
		
		private boolean bottomRightPressed;
		
		public void raiseBottomRightPressed() {
			bottomRightPressed = true;
		}
		
		private boolean bottomRightReleased;
		
		public void raiseBottomRightReleased() {
			bottomRightReleased = true;
		}
		
		protected void clearEvents() {
			topLeftPressed = false;
			topLeftReleased = false;
			topRightPressed = false;
			topRightReleased = false;
			bottomLeftPressed = false;
			bottomLeftReleased = false;
			bottomRightPressed = false;
			bottomRightReleased = false;
		}
	}
	
	protected SCIButtonsImpl sCIButtons;
	
	protected class SCIDisplayImpl implements SCIDisplay {
	
		private SCIDisplayOperationCallback operationCallback;
		
		public void setSCIDisplayOperationCallback(
				SCIDisplayOperationCallback operationCallback) {
			this.operationCallback = operationCallback;
		}
	}
	
	protected SCIDisplayImpl sCIDisplay;
	
	protected class SCILogicUnitImpl implements SCILogicUnit {
	
		private SCILogicUnitOperationCallback operationCallback;
		
		public void setSCILogicUnitOperationCallback(
				SCILogicUnitOperationCallback operationCallback) {
			this.operationCallback = operationCallback;
		}
		private boolean startAlarm;
		
		public void raiseStartAlarm() {
			startAlarm = true;
		}
		
		private long count;
		
		public long getCount() {
			return count;
		}
		
		public void setCount(long value) {
			this.count = value;
		}
		
		private long alarmCount;
		
		public long getAlarmCount() {
			return alarmCount;
		}
		
		public void setAlarmCount(long value) {
			this.alarmCount = value;
		}
		
		private boolean chronoOn;
		
		public boolean getChronoOn() {
			return chronoOn;
		}
		
		public void setChronoOn(boolean value) {
			this.chronoOn = value;
		}
		
		private boolean tickClock;
		
		public boolean getTickClock() {
			return tickClock;
		}
		
		public void setTickClock(boolean value) {
			this.tickClock = value;
		}
		
		private boolean tickChrono;
		
		public boolean getTickChrono() {
			return tickChrono;
		}
		
		public void setTickChrono(boolean value) {
			this.tickChrono = value;
		}
		
		protected void clearEvents() {
			startAlarm = false;
		}
	}
	
	protected SCILogicUnitImpl sCILogicUnit;
	
	private boolean initialized = false;
	
	public enum State {
		main_region_Alarm_Clock,
		main_region_Alarm_Clock_update_update_time_and_chrono,
		main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time,
		main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono,
		main_region_Alarm_Clock_watch_light_control,
		main_region_Alarm_Clock_watch_light_control_light_light_on,
		main_region_Alarm_Clock_watch_light_control_light_delay,
		main_region_Alarm_Clock_watch_light_control_light_light_off,
		main_region_Alarm_Clock_watch_light_control_time_update_time,
		main_region_Alarm_Clock_watch_digitalwatch,
		main_region_Alarm_Clock_watch_edit,
		main_region_Alarm_Clock_watch_edit_hide_show_hide,
		main_region_Alarm_Clock_watch_edit_hide_show_show,
		main_region_Alarm_Clock_watch_edit_increase_enter,
		main_region_Alarm_Clock_watch_edit_increase_increase_selection,
		main_region_Alarm_Clock_watch_edit_increase_released,
		main_region_Alarm_Clock_watch_edit_next_next_selection,
		main_region_Alarm_Clock_watch_edit_next_enter,
		main_region_Alarm_Clock_watch_edit_next_held,
		main_region_Alarm_Clock_watch_held_botton_right,
		main_region_Alarm_Clock_watch_edit_time,
		main_region_Alarm_Clock_watch_held_botton_left,
		main_region_Alarm_Clock_watch_edit_alam,
		main_region_Alarm_Clock_watch_alam_on,
		main_region_Alarm_Clock_watch_wait,
		main_region_Alarm_Clock_chrono_chrono_control,
		main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono,
		main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono,
		main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono,
		main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono,
		main_region_Alarm_Clock_chrono_wait,
		main_region_Alarm_Clock_alarm_alarm,
		main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off,
		main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on,
		main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt,
		main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off,
		main_region_Alarm_Clock_alarm_wait,
		$NullState$
	};
	
	private State[] historyVector = new State[1];
	private final State[] stateVector = new State[8];
	
	private int nextStateIndex;
	
	private ITimer timer;
	
	private final boolean[] timeEvents = new boolean[14];
	public DigitalwatchStatemachine() {
		sCIButtons = new SCIButtonsImpl();
		sCIDisplay = new SCIDisplayImpl();
		sCILogicUnit = new SCILogicUnitImpl();
	}
	
	public void init() {
		this.initialized = true;
		if (timer == null) {
			throw new IllegalStateException("timer not set.");
		}
		for (int i = 0; i < 8; i++) {
			stateVector[i] = State.$NullState$;
		}
		for (int i = 0; i < 1; i++) {
			historyVector[i] = State.$NullState$;
		}
		clearEvents();
		clearOutEvents();
		sCILogicUnit.setCount(0);
		
		sCILogicUnit.setAlarmCount(0);
		
		sCILogicUnit.setChronoOn(false);
		
		sCILogicUnit.setTickClock(false);
		
		sCILogicUnit.setTickChrono(false);
	}
	
	public void enter() {
		if (!initialized) {
			throw new IllegalStateException(
					"The state machine needs to be initialized first by calling the init() function.");
		}
		if (timer == null) {
			throw new IllegalStateException("timer not set.");
		}
		enterSequence_main_region_default();
	}
	
	public void exit() {
		exitSequence_main_region();
	}
	
	/**
	 * @see IStatemachine#isActive()
	 */
	public boolean isActive() {
		return stateVector[0] != State.$NullState$||stateVector[1] != State.$NullState$||stateVector[2] != State.$NullState$||stateVector[3] != State.$NullState$||stateVector[4] != State.$NullState$||stateVector[5] != State.$NullState$||stateVector[6] != State.$NullState$||stateVector[7] != State.$NullState$;
	}
	
	/** 
	* Always returns 'false' since this state machine can never become final.
	*
	* @see IStatemachine#isFinal()
	*/
	public boolean isFinal() {
		return false;
	}
	/**
	* This method resets the incoming events (time events included).
	*/
	protected void clearEvents() {
		sCIButtons.clearEvents();
		sCILogicUnit.clearEvents();
		for (int i=0; i<timeEvents.length; i++) {
			timeEvents[i] = false;
		}
	}
	
	/**
	* This method resets the outgoing events.
	*/
	protected void clearOutEvents() {
	}
	
	/**
	* Returns true if the given state is currently active otherwise false.
	*/
	public boolean isStateActive(State state) {
	
		switch (state) {
		case main_region_Alarm_Clock:
			return stateVector[0].ordinal() >= State.
					main_region_Alarm_Clock.ordinal()&& stateVector[0].ordinal() <= State.main_region_Alarm_Clock_alarm_wait.ordinal();
		case main_region_Alarm_Clock_update_update_time_and_chrono:
			return stateVector[0].ordinal() >= State.
					main_region_Alarm_Clock_update_update_time_and_chrono.ordinal()&& stateVector[0].ordinal() <= State.main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono.ordinal();
		case main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time:
			return stateVector[0] == State.main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time;
		case main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono:
			return stateVector[1] == State.main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono;
		case main_region_Alarm_Clock_watch_light_control:
			return stateVector[2].ordinal() >= State.
					main_region_Alarm_Clock_watch_light_control.ordinal()&& stateVector[2].ordinal() <= State.main_region_Alarm_Clock_watch_light_control_time_update_time.ordinal();
		case main_region_Alarm_Clock_watch_light_control_light_light_on:
			return stateVector[2] == State.main_region_Alarm_Clock_watch_light_control_light_light_on;
		case main_region_Alarm_Clock_watch_light_control_light_delay:
			return stateVector[2] == State.main_region_Alarm_Clock_watch_light_control_light_delay;
		case main_region_Alarm_Clock_watch_light_control_light_light_off:
			return stateVector[2] == State.main_region_Alarm_Clock_watch_light_control_light_light_off;
		case main_region_Alarm_Clock_watch_light_control_time_update_time:
			return stateVector[3] == State.main_region_Alarm_Clock_watch_light_control_time_update_time;
		case main_region_Alarm_Clock_watch_digitalwatch:
			return stateVector[2] == State.main_region_Alarm_Clock_watch_digitalwatch;
		case main_region_Alarm_Clock_watch_edit:
			return stateVector[2].ordinal() >= State.
					main_region_Alarm_Clock_watch_edit.ordinal()&& stateVector[2].ordinal() <= State.main_region_Alarm_Clock_watch_edit_next_held.ordinal();
		case main_region_Alarm_Clock_watch_edit_hide_show_hide:
			return stateVector[2] == State.main_region_Alarm_Clock_watch_edit_hide_show_hide;
		case main_region_Alarm_Clock_watch_edit_hide_show_show:
			return stateVector[2] == State.main_region_Alarm_Clock_watch_edit_hide_show_show;
		case main_region_Alarm_Clock_watch_edit_increase_enter:
			return stateVector[3] == State.main_region_Alarm_Clock_watch_edit_increase_enter;
		case main_region_Alarm_Clock_watch_edit_increase_increase_selection:
			return stateVector[3] == State.main_region_Alarm_Clock_watch_edit_increase_increase_selection;
		case main_region_Alarm_Clock_watch_edit_increase_released:
			return stateVector[3] == State.main_region_Alarm_Clock_watch_edit_increase_released;
		case main_region_Alarm_Clock_watch_edit_next_next_selection:
			return stateVector[4] == State.main_region_Alarm_Clock_watch_edit_next_next_selection;
		case main_region_Alarm_Clock_watch_edit_next_enter:
			return stateVector[4] == State.main_region_Alarm_Clock_watch_edit_next_enter;
		case main_region_Alarm_Clock_watch_edit_next_held:
			return stateVector[4] == State.main_region_Alarm_Clock_watch_edit_next_held;
		case main_region_Alarm_Clock_watch_held_botton_right:
			return stateVector[2] == State.main_region_Alarm_Clock_watch_held_botton_right;
		case main_region_Alarm_Clock_watch_edit_time:
			return stateVector[2] == State.main_region_Alarm_Clock_watch_edit_time;
		case main_region_Alarm_Clock_watch_held_botton_left:
			return stateVector[2] == State.main_region_Alarm_Clock_watch_held_botton_left;
		case main_region_Alarm_Clock_watch_edit_alam:
			return stateVector[2] == State.main_region_Alarm_Clock_watch_edit_alam;
		case main_region_Alarm_Clock_watch_alam_on:
			return stateVector[2] == State.main_region_Alarm_Clock_watch_alam_on;
		case main_region_Alarm_Clock_watch_wait:
			return stateVector[2] == State.main_region_Alarm_Clock_watch_wait;
		case main_region_Alarm_Clock_chrono_chrono_control:
			return stateVector[5].ordinal() >= State.
					main_region_Alarm_Clock_chrono_chrono_control.ordinal()&& stateVector[5].ordinal() <= State.main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono.ordinal();
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono:
			return stateVector[5] == State.main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono:
			return stateVector[5] == State.main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono:
			return stateVector[5] == State.main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono:
			return stateVector[5] == State.main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono;
		case main_region_Alarm_Clock_chrono_wait:
			return stateVector[5] == State.main_region_Alarm_Clock_chrono_wait;
		case main_region_Alarm_Clock_alarm_alarm:
			return stateVector[6].ordinal() >= State.
					main_region_Alarm_Clock_alarm_alarm.ordinal()&& stateVector[6].ordinal() <= State.main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off.ordinal();
		case main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off:
			return stateVector[6] == State.main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off;
		case main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on:
			return stateVector[6] == State.main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on;
		case main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt:
			return stateVector[7] == State.main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt;
		case main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off:
			return stateVector[7] == State.main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off;
		case main_region_Alarm_Clock_alarm_wait:
			return stateVector[6] == State.main_region_Alarm_Clock_alarm_wait;
		default:
			return false;
		}
	}
	
	/**
	* Set the {@link ITimer} for the state machine. It must be set
	* externally on a timed state machine before a run cycle can be correct
	* executed.
	* 
	* @param timer
	*/
	public void setTimer(ITimer timer) {
		this.timer = timer;
	}
	
	/**
	* Returns the currently used timer.
	* 
	* @return {@link ITimer}
	*/
	public ITimer getTimer() {
		return timer;
	}
	
	public void timeElapsed(int eventID) {
		timeEvents[eventID] = true;
	}
	
	public SCIButtons getSCIButtons() {
		return sCIButtons;
	}
	
	public SCIDisplay getSCIDisplay() {
		return sCIDisplay;
	}
	
	public SCILogicUnit getSCILogicUnit() {
		return sCILogicUnit;
	}
	
	private boolean check_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time_tr0_tr0() {
		return (timeEvents[0]) && (sCILogicUnit.tickClock==true);
	}
	
	private boolean check_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono_tr0_tr0() {
		return (timeEvents[1]) && (sCILogicUnit.tickChrono==true);
	}
	
	private boolean check_main_region_Alarm_Clock_watch_light_control_light_light_on_tr0_tr0() {
		return sCIButtons.topRightReleased;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_light_control_light_delay_tr0_tr0() {
		return sCIButtons.topRightPressed;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_light_control_light_delay_tr1_tr1() {
		return timeEvents[2];
	}
	
	private boolean check_main_region_Alarm_Clock_watch_light_control_light_light_off_tr0_tr0() {
		return true;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_light_control_time_update_time_tr0_tr0() {
		return timeEvents[3];
	}
	
	private boolean check_main_region_Alarm_Clock_watch_digitalwatch_tr0_tr0() {
		return sCIButtons.bottomRightPressed;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_digitalwatch_tr1_tr1() {
		return timeEvents[4];
	}
	
	private boolean check_main_region_Alarm_Clock_watch_digitalwatch_tr2_tr2() {
		return sCIButtons.topRightPressed;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_digitalwatch_tr3_tr3() {
		return sCIButtons.bottomLeftPressed;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_digitalwatch_tr4_tr4() {
		return sCILogicUnit.chronoOn==true;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_edit_hide_show_hide_tr0_tr0() {
		return (timeEvents[5]) && (sCILogicUnit.count<=8);
	}
	
	private boolean check_main_region_Alarm_Clock_watch_edit_hide_show_hide_tr1_tr1() {
		return sCILogicUnit.count>8;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_edit_hide_show_show_tr0_tr0() {
		return (timeEvents[6]) && (sCILogicUnit.count!=-1);
	}
	
	private boolean check_main_region_Alarm_Clock_watch_edit_increase_enter_tr0_tr0() {
		return sCIButtons.bottomLeftPressed;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_edit_increase_increase_selection_tr0_tr0() {
		return timeEvents[7];
	}
	
	private boolean check_main_region_Alarm_Clock_watch_edit_increase_increase_selection_tr1_tr1() {
		return sCIButtons.bottomLeftReleased;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_edit_increase_released_tr0_tr0() {
		return true;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_edit_next_next_selection_tr0_tr0() {
		return sCIButtons.bottomRightReleased;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_edit_next_next_selection_tr1_tr1() {
		return timeEvents[8];
	}
	
	private boolean check_main_region_Alarm_Clock_watch_edit_next_enter_tr0_tr0() {
		return sCIButtons.bottomRightPressed;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_edit_next_held_tr0_tr0() {
		return true;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_held_botton_right_tr0_tr0() {
		return sCIButtons.bottomRightReleased;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_held_botton_right_tr1_tr1() {
		return timeEvents[9];
	}
	
	private boolean check_main_region_Alarm_Clock_watch_edit_time_tr0_tr0() {
		return true;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_held_botton_left_tr0_tr0() {
		return timeEvents[10];
	}
	
	private boolean check_main_region_Alarm_Clock_watch_held_botton_left_tr1_tr1() {
		return sCIButtons.bottomLeftReleased;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_edit_alam_tr0_tr0() {
		return true;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_alam_on_tr0_tr0() {
		return true;
	}
	
	private boolean check_main_region_Alarm_Clock_watch_wait_tr0_tr0() {
		return sCILogicUnit.chronoOn==false;
	}
	
	private boolean check_main_region_Alarm_Clock_chrono_chrono_control_tr0_tr0() {
		return sCIButtons.topLeftPressed;
	}
	
	private boolean check_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono_tr0_tr0() {
		return sCIButtons.bottomRightPressed;
	}
	
	private boolean check_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono_tr0_tr0() {
		return true;
	}
	
	private boolean check_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono_tr0_tr0() {
		return sCIButtons.bottomRightPressed;
	}
	
	private boolean check_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono_tr1_tr1() {
		return sCIButtons.bottomLeftPressed;
	}
	
	private boolean check_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono_tr0_tr0() {
		return timeEvents[11];
	}
	
	private boolean check_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono_tr1_tr1() {
		return sCIButtons.bottomRightPressed;
	}
	
	private boolean check_main_region_Alarm_Clock_chrono_wait_tr0_tr0() {
		return sCIButtons.topLeftPressed;
	}
	
	private boolean check_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off_tr0_tr0() {
		return timeEvents[12];
	}
	
	private boolean check_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off_tr1_tr1() {
		return sCILogicUnit.alarmCount>3;
	}
	
	private boolean check_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on_tr0_tr0() {
		return timeEvents[13];
	}
	
	private boolean check_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt_tr0_tr0() {
		return sCIButtons.topLeftPressed || sCIButtons.bottomLeftPressed || sCIButtons.topRightPressed || sCIButtons.bottomRightPressed;
	}
	
	private boolean check_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off_tr0_tr0() {
		return true;
	}
	
	private boolean check_main_region_Alarm_Clock_alarm_wait_tr0_tr0() {
		return sCILogicUnit.startAlarm;
	}
	
	private void effect_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time_tr0() {
		exitSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time();
		enterSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time_default();
	}
	
	private void effect_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono_tr0() {
		exitSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono();
		enterSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_light_control_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_light_control();
		enterSequence_main_region_Alarm_Clock_watch_digitalwatch_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_light_control_light_light_on_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_light_control_light_light_on();
		enterSequence_main_region_Alarm_Clock_watch_light_control_light_delay_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_light_control_light_delay_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_light_control_light_delay();
		enterSequence_main_region_Alarm_Clock_watch_light_control_light_light_on_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_light_control_light_delay_tr1() {
		exitSequence_main_region_Alarm_Clock_watch_light_control_light_delay();
		enterSequence_main_region_Alarm_Clock_watch_light_control_light_light_off_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_light_control_light_light_off_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_light_control_light_light_off();
		react_main_region_Alarm_Clock_watch_light_control_light__exit_Default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_light_control_time_update_time_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_light_control_time_update_time();
		enterSequence_main_region_Alarm_Clock_watch_light_control_time_update_time_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_digitalwatch_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_digitalwatch();
		enterSequence_main_region_Alarm_Clock_watch_held_botton_right_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_digitalwatch_tr1() {
		exitSequence_main_region_Alarm_Clock_watch_digitalwatch();
		enterSequence_main_region_Alarm_Clock_watch_digitalwatch_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_digitalwatch_tr2() {
		exitSequence_main_region_Alarm_Clock_watch_digitalwatch();
		enterSequence_main_region_Alarm_Clock_watch_light_control_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_digitalwatch_tr3() {
		exitSequence_main_region_Alarm_Clock_watch_digitalwatch();
		enterSequence_main_region_Alarm_Clock_watch_held_botton_left_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_digitalwatch_tr4() {
		exitSequence_main_region_Alarm_Clock_watch_digitalwatch();
		enterSequence_main_region_Alarm_Clock_watch_wait_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_edit();
		enterSequence_main_region_Alarm_Clock_watch_digitalwatch_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_hide_show_hide_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_edit_hide_show_hide();
		enterSequence_main_region_Alarm_Clock_watch_edit_hide_show_show_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_hide_show_hide_tr1() {
		exitSequence_main_region_Alarm_Clock_watch_edit_hide_show_hide();
		react_main_region_Alarm_Clock_watch_edit_hide_show__exit_Default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_hide_show_show_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_edit_hide_show_show();
		enterSequence_main_region_Alarm_Clock_watch_edit_hide_show_hide_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_increase_enter_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_edit_increase_enter();
		enterSequence_main_region_Alarm_Clock_watch_edit_increase_increase_selection_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_increase_increase_selection_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_edit_increase_increase_selection();
		enterSequence_main_region_Alarm_Clock_watch_edit_increase_increase_selection_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_increase_increase_selection_tr1() {
		exitSequence_main_region_Alarm_Clock_watch_edit_increase_increase_selection();
		enterSequence_main_region_Alarm_Clock_watch_edit_increase_released_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_increase_released_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_edit_increase_released();
		enterSequence_main_region_Alarm_Clock_watch_edit_increase_enter_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_next_next_selection_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_edit_next_next_selection();
		enterSequence_main_region_Alarm_Clock_watch_edit_next_enter_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_next_next_selection_tr1() {
		exitSequence_main_region_Alarm_Clock_watch_edit_next_next_selection();
		enterSequence_main_region_Alarm_Clock_watch_edit_next_held_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_next_enter_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_edit_next_enter();
		enterSequence_main_region_Alarm_Clock_watch_edit_next_next_selection_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_next_held_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_edit_next_held();
		react_main_region_Alarm_Clock_watch_edit_next__exit_Default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_held_botton_right_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_held_botton_right();
		enterSequence_main_region_Alarm_Clock_watch_digitalwatch_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_held_botton_right_tr1() {
		exitSequence_main_region_Alarm_Clock_watch_held_botton_right();
		enterSequence_main_region_Alarm_Clock_watch_edit_time_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_time_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_edit_time();
		enterSequence_main_region_Alarm_Clock_watch_edit_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_held_botton_left_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_held_botton_left();
		enterSequence_main_region_Alarm_Clock_watch_edit_alam_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_held_botton_left_tr1() {
		exitSequence_main_region_Alarm_Clock_watch_held_botton_left();
		enterSequence_main_region_Alarm_Clock_watch_alam_on_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_edit_alam_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_edit_alam();
		enterSequence_main_region_Alarm_Clock_watch_edit_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_alam_on_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_alam_on();
		enterSequence_main_region_Alarm_Clock_watch_digitalwatch_default();
	}
	
	private void effect_main_region_Alarm_Clock_watch_wait_tr0() {
		exitSequence_main_region_Alarm_Clock_watch_wait();
		enterSequence_main_region_Alarm_Clock_watch_digitalwatch_default();
	}
	
	private void effect_main_region_Alarm_Clock_chrono_chrono_control_tr0() {
		exitSequence_main_region_Alarm_Clock_chrono_chrono_control();
		enterSequence_main_region_Alarm_Clock_chrono_wait_default();
	}
	
	private void effect_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono_tr0() {
		exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono();
		sCILogicUnit.setTickChrono(true);
		
		enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono_default();
	}
	
	private void effect_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono_tr0() {
		exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono();
		enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono_default();
	}
	
	private void effect_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono_tr0() {
		exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono();
		sCILogicUnit.setTickChrono(true);
		
		enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono_default();
	}
	
	private void effect_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono_tr1() {
		exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono();
		sCILogicUnit.setTickChrono(false);
		
		enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono_default();
	}
	
	private void effect_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono_tr0() {
		exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono();
		enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono_default();
	}
	
	private void effect_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono_tr1() {
		exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono();
		sCILogicUnit.setTickChrono(false);
		
		enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono_default();
	}
	
	private void effect_main_region_Alarm_Clock_chrono_wait_tr0() {
		exitSequence_main_region_Alarm_Clock_chrono_wait();
		enterSequence_main_region_Alarm_Clock_chrono_chrono_control_default();
	}
	
	private void effect_main_region_Alarm_Clock_alarm_alarm_tr0() {
		exitSequence_main_region_Alarm_Clock_alarm_alarm();
		enterSequence_main_region_Alarm_Clock_alarm_wait_default();
	}
	
	private void effect_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off_tr0() {
		exitSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off();
		enterSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on_default();
	}
	
	private void effect_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off_tr1() {
		exitSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off();
		react_main_region_Alarm_Clock_alarm_alarm_activate_alarm__exit_Default();
	}
	
	private void effect_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on_tr0() {
		exitSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on();
		enterSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off_default();
	}
	
	private void effect_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt_tr0() {
		exitSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt();
		enterSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off_default();
	}
	
	private void effect_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off_tr0() {
		exitSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off();
		react_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm__exit_Default();
	}
	
	private void effect_main_region_Alarm_Clock_alarm_wait_tr0() {
		exitSequence_main_region_Alarm_Clock_alarm_wait();
		enterSequence_main_region_Alarm_Clock_alarm_alarm_default();
	}
	
	/* Entry action for state 'Alarm Clock'. */
	private void entryAction_main_region_Alarm_Clock() {
		sCILogicUnit.setChronoOn(false);
	}
	
	/* Entry action for state 'update time and chrono'. */
	private void entryAction_main_region_Alarm_Clock_update_update_time_and_chrono() {
		sCILogicUnit.setTickClock(true);
	}
	
	/* Entry action for state 'tick time'. */
	private void entryAction_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time() {
		timer.setTimer(this, 0, 1*1000, true);
		
		sCILogicUnit.operationCallback.increaseTimeByOne();
	}
	
	/* Entry action for state 'tick chrono'. */
	private void entryAction_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono() {
		timer.setTimer(this, 1, 1, true);
	}
	
	/* Entry action for state 'light on'. */
	private void entryAction_main_region_Alarm_Clock_watch_light_control_light_light_on() {
		sCIDisplay.operationCallback.setIndiglo();
	}
	
	/* Entry action for state 'delay'. */
	private void entryAction_main_region_Alarm_Clock_watch_light_control_light_delay() {
		timer.setTimer(this, 2, 2*1000, false);
	}
	
	/* Entry action for state 'light off'. */
	private void entryAction_main_region_Alarm_Clock_watch_light_control_light_light_off() {
		sCIDisplay.operationCallback.unsetIndiglo();
	}
	
	/* Entry action for state 'update time'. */
	private void entryAction_main_region_Alarm_Clock_watch_light_control_time_update_time() {
		timer.setTimer(this, 3, 1*1000, true);
		
		sCIDisplay.operationCallback.refreshTimeDisplay();
		
		sCIDisplay.operationCallback.refreshDateDisplay();
	}
	
	/* Entry action for state 'digitalwatch'. */
	private void entryAction_main_region_Alarm_Clock_watch_digitalwatch() {
		timer.setTimer(this, 4, 1*1000, true);
		
		sCIDisplay.operationCallback.refreshTimeDisplay();
		
		sCIDisplay.operationCallback.refreshDateDisplay();
	}
	
	/* Entry action for state 'edit'. */
	private void entryAction_main_region_Alarm_Clock_watch_edit() {
		sCILogicUnit.setCount(0);
	}
	
	/* Entry action for state 'hide'. */
	private void entryAction_main_region_Alarm_Clock_watch_edit_hide_show_hide() {
		timer.setTimer(this, 5, 312, false);
		
		sCILogicUnit.setCount(sCILogicUnit.getCount() + 1);
		
		sCIDisplay.operationCallback.hideSelection();
	}
	
	/* Entry action for state 'show'. */
	private void entryAction_main_region_Alarm_Clock_watch_edit_hide_show_show() {
		timer.setTimer(this, 6, 312, false);
		
		sCIDisplay.operationCallback.showSelection();
	}
	
	/* Entry action for state 'increase selection'. */
	private void entryAction_main_region_Alarm_Clock_watch_edit_increase_increase_selection() {
		timer.setTimer(this, 7, 300, true);
		
		sCILogicUnit.operationCallback.increaseSelection();
		
		sCIDisplay.operationCallback.showSelection();
		
		sCILogicUnit.setCount(-1);
	}
	
	/* Entry action for state 'released'. */
	private void entryAction_main_region_Alarm_Clock_watch_edit_increase_released() {
		sCILogicUnit.setCount(0);
	}
	
	/* Entry action for state 'next selection'. */
	private void entryAction_main_region_Alarm_Clock_watch_edit_next_next_selection() {
		timer.setTimer(this, 8, 2*1000, false);
		
		sCILogicUnit.setCount(0);
		
		sCILogicUnit.operationCallback.selectNext();
	}
	
	/* Entry action for state 'held botton right'. */
	private void entryAction_main_region_Alarm_Clock_watch_held_botton_right() {
		timer.setTimer(this, 9, 1500, false);
	}
	
	/* Entry action for state 'edit time'. */
	private void entryAction_main_region_Alarm_Clock_watch_edit_time() {
		sCILogicUnit.operationCallback.startTimeEditMode();
		
		sCILogicUnit.setTickClock(false);
	}
	
	/* Entry action for state 'held botton left'. */
	private void entryAction_main_region_Alarm_Clock_watch_held_botton_left() {
		timer.setTimer(this, 10, 1500, false);
	}
	
	/* Entry action for state 'edit alam'. */
	private void entryAction_main_region_Alarm_Clock_watch_edit_alam() {
		sCILogicUnit.operationCallback.startAlarmEditMode();
	}
	
	/* Entry action for state 'alam on'. */
	private void entryAction_main_region_Alarm_Clock_watch_alam_on() {
		sCILogicUnit.operationCallback.setAlarm();
		
		sCIDisplay.operationCallback.refreshAlarmDisplay();
	}
	
	/* Entry action for state 'chrono control'. */
	private void entryAction_main_region_Alarm_Clock_chrono_chrono_control() {
		sCILogicUnit.setChronoOn(true);
	}
	
	/* Entry action for state 'display chrono'. */
	private void entryAction_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono() {
		sCIDisplay.operationCallback.refreshChronoDisplay();
	}
	
	/* Entry action for state 'reset chrono'. */
	private void entryAction_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono() {
		sCILogicUnit.operationCallback.resetChrono();
		
		sCIDisplay.operationCallback.refreshChronoDisplay();
	}
	
	/* Entry action for state 'pause chrono'. */
	private void entryAction_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono() {
		sCIDisplay.operationCallback.refreshChronoDisplay();
	}
	
	/* Entry action for state 'start chrono'. */
	private void entryAction_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono() {
		timer.setTimer(this, 11, 1, true);
		
		sCIDisplay.operationCallback.refreshChronoDisplay();
	}
	
	/* Entry action for state 'alarm'. */
	private void entryAction_main_region_Alarm_Clock_alarm_alarm() {
		sCILogicUnit.setAlarmCount(0);
	}
	
	/* Entry action for state 'light off'. */
	private void entryAction_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off() {
		timer.setTimer(this, 12, 500, false);
		
		sCIDisplay.operationCallback.unsetIndiglo();
	}
	
	/* Entry action for state 'light on'. */
	private void entryAction_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on() {
		timer.setTimer(this, 13, 500, false);
		
		sCIDisplay.operationCallback.setIndiglo();
		
		sCILogicUnit.setAlarmCount(sCILogicUnit.getAlarmCount() + 1);
	}
	
	/* Entry action for state 'light off'. */
	private void entryAction_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off() {
		sCIDisplay.operationCallback.unsetIndiglo();
	}
	
	/* Exit action for state 'tick time'. */
	private void exitAction_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time() {
		timer.unsetTimer(this, 0);
	}
	
	/* Exit action for state 'tick chrono'. */
	private void exitAction_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono() {
		timer.unsetTimer(this, 1);
		
		sCILogicUnit.operationCallback.increaseChronoByOne();
	}
	
	/* Exit action for state 'delay'. */
	private void exitAction_main_region_Alarm_Clock_watch_light_control_light_delay() {
		timer.unsetTimer(this, 2);
	}
	
	/* Exit action for state 'update time'. */
	private void exitAction_main_region_Alarm_Clock_watch_light_control_time_update_time() {
		timer.unsetTimer(this, 3);
	}
	
	/* Exit action for state 'digitalwatch'. */
	private void exitAction_main_region_Alarm_Clock_watch_digitalwatch() {
		timer.unsetTimer(this, 4);
	}
	
	/* Exit action for state 'edit'. */
	private void exitAction_main_region_Alarm_Clock_watch_edit() {
		sCILogicUnit.setTickClock(true);
	}
	
	/* Exit action for state 'hide'. */
	private void exitAction_main_region_Alarm_Clock_watch_edit_hide_show_hide() {
		timer.unsetTimer(this, 5);
	}
	
	/* Exit action for state 'show'. */
	private void exitAction_main_region_Alarm_Clock_watch_edit_hide_show_show() {
		timer.unsetTimer(this, 6);
	}
	
	/* Exit action for state 'increase selection'. */
	private void exitAction_main_region_Alarm_Clock_watch_edit_increase_increase_selection() {
		timer.unsetTimer(this, 7);
	}
	
	/* Exit action for state 'next selection'. */
	private void exitAction_main_region_Alarm_Clock_watch_edit_next_next_selection() {
		timer.unsetTimer(this, 8);
	}
	
	/* Exit action for state 'held botton right'. */
	private void exitAction_main_region_Alarm_Clock_watch_held_botton_right() {
		timer.unsetTimer(this, 9);
	}
	
	/* Exit action for state 'held botton left'. */
	private void exitAction_main_region_Alarm_Clock_watch_held_botton_left() {
		timer.unsetTimer(this, 10);
	}
	
	/* Exit action for state 'chrono control'. */
	private void exitAction_main_region_Alarm_Clock_chrono_chrono_control() {
		sCILogicUnit.setChronoOn(false);
	}
	
	/* Exit action for state 'start chrono'. */
	private void exitAction_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono() {
		timer.unsetTimer(this, 11);
	}
	
	/* Exit action for state 'alarm'. */
	private void exitAction_main_region_Alarm_Clock_alarm_alarm() {
		sCILogicUnit.operationCallback.setAlarm();
		
		sCIDisplay.operationCallback.refreshAlarmDisplay();
	}
	
	/* Exit action for state 'light off'. */
	private void exitAction_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off() {
		timer.unsetTimer(this, 12);
	}
	
	/* Exit action for state 'light on'. */
	private void exitAction_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on() {
		timer.unsetTimer(this, 13);
	}
	
	/* 'default' enter sequence for state Alarm Clock */
	private void enterSequence_main_region_Alarm_Clock_default() {
		entryAction_main_region_Alarm_Clock();
		enterSequence_main_region_Alarm_Clock_update_default();
		enterSequence_main_region_Alarm_Clock_watch_default();
		enterSequence_main_region_Alarm_Clock_chrono_default();
		enterSequence_main_region_Alarm_Clock_alarm_default();
	}
	
	/* 'default' enter sequence for state update time and chrono */
	private void enterSequence_main_region_Alarm_Clock_update_update_time_and_chrono_default() {
		entryAction_main_region_Alarm_Clock_update_update_time_and_chrono();
		enterSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_default();
		enterSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_default();
	}
	
	/* 'default' enter sequence for state tick time */
	private void enterSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time_default() {
		entryAction_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time();
		nextStateIndex = 0;
		stateVector[0] = State.main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time;
	}
	
	/* 'default' enter sequence for state tick chrono */
	private void enterSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono_default() {
		entryAction_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono();
		nextStateIndex = 1;
		stateVector[1] = State.main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono;
	}
	
	/* 'default' enter sequence for state light control */
	private void enterSequence_main_region_Alarm_Clock_watch_light_control_default() {
		enterSequence_main_region_Alarm_Clock_watch_light_control_light_default();
		enterSequence_main_region_Alarm_Clock_watch_light_control_time_default();
	}
	
	/* 'default' enter sequence for state light on */
	private void enterSequence_main_region_Alarm_Clock_watch_light_control_light_light_on_default() {
		entryAction_main_region_Alarm_Clock_watch_light_control_light_light_on();
		nextStateIndex = 2;
		stateVector[2] = State.main_region_Alarm_Clock_watch_light_control_light_light_on;
	}
	
	/* 'default' enter sequence for state delay */
	private void enterSequence_main_region_Alarm_Clock_watch_light_control_light_delay_default() {
		entryAction_main_region_Alarm_Clock_watch_light_control_light_delay();
		nextStateIndex = 2;
		stateVector[2] = State.main_region_Alarm_Clock_watch_light_control_light_delay;
	}
	
	/* 'default' enter sequence for state light off */
	private void enterSequence_main_region_Alarm_Clock_watch_light_control_light_light_off_default() {
		entryAction_main_region_Alarm_Clock_watch_light_control_light_light_off();
		nextStateIndex = 2;
		stateVector[2] = State.main_region_Alarm_Clock_watch_light_control_light_light_off;
	}
	
	/* 'default' enter sequence for state update time */
	private void enterSequence_main_region_Alarm_Clock_watch_light_control_time_update_time_default() {
		entryAction_main_region_Alarm_Clock_watch_light_control_time_update_time();
		nextStateIndex = 3;
		stateVector[3] = State.main_region_Alarm_Clock_watch_light_control_time_update_time;
	}
	
	/* 'default' enter sequence for state digitalwatch */
	private void enterSequence_main_region_Alarm_Clock_watch_digitalwatch_default() {
		entryAction_main_region_Alarm_Clock_watch_digitalwatch();
		nextStateIndex = 2;
		stateVector[2] = State.main_region_Alarm_Clock_watch_digitalwatch;
	}
	
	/* 'default' enter sequence for state edit */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_default() {
		entryAction_main_region_Alarm_Clock_watch_edit();
		enterSequence_main_region_Alarm_Clock_watch_edit_hide_show_default();
		enterSequence_main_region_Alarm_Clock_watch_edit_increase_default();
		enterSequence_main_region_Alarm_Clock_watch_edit_next_default();
	}
	
	/* 'default' enter sequence for state hide */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_hide_show_hide_default() {
		entryAction_main_region_Alarm_Clock_watch_edit_hide_show_hide();
		nextStateIndex = 2;
		stateVector[2] = State.main_region_Alarm_Clock_watch_edit_hide_show_hide;
	}
	
	/* 'default' enter sequence for state show */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_hide_show_show_default() {
		entryAction_main_region_Alarm_Clock_watch_edit_hide_show_show();
		nextStateIndex = 2;
		stateVector[2] = State.main_region_Alarm_Clock_watch_edit_hide_show_show;
	}
	
	/* 'default' enter sequence for state enter */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_increase_enter_default() {
		nextStateIndex = 3;
		stateVector[3] = State.main_region_Alarm_Clock_watch_edit_increase_enter;
	}
	
	/* 'default' enter sequence for state increase selection */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_increase_increase_selection_default() {
		entryAction_main_region_Alarm_Clock_watch_edit_increase_increase_selection();
		nextStateIndex = 3;
		stateVector[3] = State.main_region_Alarm_Clock_watch_edit_increase_increase_selection;
	}
	
	/* 'default' enter sequence for state released */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_increase_released_default() {
		entryAction_main_region_Alarm_Clock_watch_edit_increase_released();
		nextStateIndex = 3;
		stateVector[3] = State.main_region_Alarm_Clock_watch_edit_increase_released;
	}
	
	/* 'default' enter sequence for state next selection */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_next_next_selection_default() {
		entryAction_main_region_Alarm_Clock_watch_edit_next_next_selection();
		nextStateIndex = 4;
		stateVector[4] = State.main_region_Alarm_Clock_watch_edit_next_next_selection;
	}
	
	/* 'default' enter sequence for state enter */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_next_enter_default() {
		nextStateIndex = 4;
		stateVector[4] = State.main_region_Alarm_Clock_watch_edit_next_enter;
	}
	
	/* 'default' enter sequence for state held */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_next_held_default() {
		nextStateIndex = 4;
		stateVector[4] = State.main_region_Alarm_Clock_watch_edit_next_held;
	}
	
	/* 'default' enter sequence for state held botton right */
	private void enterSequence_main_region_Alarm_Clock_watch_held_botton_right_default() {
		entryAction_main_region_Alarm_Clock_watch_held_botton_right();
		nextStateIndex = 2;
		stateVector[2] = State.main_region_Alarm_Clock_watch_held_botton_right;
	}
	
	/* 'default' enter sequence for state edit time */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_time_default() {
		entryAction_main_region_Alarm_Clock_watch_edit_time();
		nextStateIndex = 2;
		stateVector[2] = State.main_region_Alarm_Clock_watch_edit_time;
	}
	
	/* 'default' enter sequence for state held botton left */
	private void enterSequence_main_region_Alarm_Clock_watch_held_botton_left_default() {
		entryAction_main_region_Alarm_Clock_watch_held_botton_left();
		nextStateIndex = 2;
		stateVector[2] = State.main_region_Alarm_Clock_watch_held_botton_left;
	}
	
	/* 'default' enter sequence for state edit alam */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_alam_default() {
		entryAction_main_region_Alarm_Clock_watch_edit_alam();
		nextStateIndex = 2;
		stateVector[2] = State.main_region_Alarm_Clock_watch_edit_alam;
	}
	
	/* 'default' enter sequence for state alam on */
	private void enterSequence_main_region_Alarm_Clock_watch_alam_on_default() {
		entryAction_main_region_Alarm_Clock_watch_alam_on();
		nextStateIndex = 2;
		stateVector[2] = State.main_region_Alarm_Clock_watch_alam_on;
	}
	
	/* 'default' enter sequence for state wait */
	private void enterSequence_main_region_Alarm_Clock_watch_wait_default() {
		nextStateIndex = 2;
		stateVector[2] = State.main_region_Alarm_Clock_watch_wait;
	}
	
	/* 'default' enter sequence for state chrono control */
	private void enterSequence_main_region_Alarm_Clock_chrono_chrono_control_default() {
		entryAction_main_region_Alarm_Clock_chrono_chrono_control();
		enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_default();
	}
	
	/* 'default' enter sequence for state display chrono */
	private void enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono_default() {
		entryAction_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono();
		nextStateIndex = 5;
		stateVector[5] = State.main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono;
		
		historyVector[0] = stateVector[5];
	}
	
	/* 'default' enter sequence for state reset chrono */
	private void enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono_default() {
		entryAction_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono();
		nextStateIndex = 5;
		stateVector[5] = State.main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono;
		
		historyVector[0] = stateVector[5];
	}
	
	/* 'default' enter sequence for state pause chrono */
	private void enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono_default() {
		entryAction_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono();
		nextStateIndex = 5;
		stateVector[5] = State.main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono;
		
		historyVector[0] = stateVector[5];
	}
	
	/* 'default' enter sequence for state start chrono */
	private void enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono_default() {
		entryAction_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono();
		nextStateIndex = 5;
		stateVector[5] = State.main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono;
		
		historyVector[0] = stateVector[5];
	}
	
	/* 'default' enter sequence for state wait */
	private void enterSequence_main_region_Alarm_Clock_chrono_wait_default() {
		nextStateIndex = 5;
		stateVector[5] = State.main_region_Alarm_Clock_chrono_wait;
	}
	
	/* 'default' enter sequence for state alarm */
	private void enterSequence_main_region_Alarm_Clock_alarm_alarm_default() {
		entryAction_main_region_Alarm_Clock_alarm_alarm();
		enterSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_default();
		enterSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_default();
	}
	
	/* 'default' enter sequence for state light off */
	private void enterSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off_default() {
		entryAction_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off();
		nextStateIndex = 6;
		stateVector[6] = State.main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off;
	}
	
	/* 'default' enter sequence for state light on */
	private void enterSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on_default() {
		entryAction_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on();
		nextStateIndex = 6;
		stateVector[6] = State.main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on;
	}
	
	/* 'default' enter sequence for state user interrupt */
	private void enterSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt_default() {
		nextStateIndex = 7;
		stateVector[7] = State.main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt;
	}
	
	/* 'default' enter sequence for state light off */
	private void enterSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off_default() {
		entryAction_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off();
		nextStateIndex = 7;
		stateVector[7] = State.main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off;
	}
	
	/* 'default' enter sequence for state wait */
	private void enterSequence_main_region_Alarm_Clock_alarm_wait_default() {
		nextStateIndex = 6;
		stateVector[6] = State.main_region_Alarm_Clock_alarm_wait;
	}
	
	/* 'default' enter sequence for region main region */
	private void enterSequence_main_region_default() {
		react_main_region__entry_Default();
	}
	
	/* 'default' enter sequence for region update */
	private void enterSequence_main_region_Alarm_Clock_update_default() {
		react_main_region_Alarm_Clock_update__entry_Default();
	}
	
	/* 'default' enter sequence for region update time */
	private void enterSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_default() {
		react_main_region_Alarm_Clock_update_update_time_and_chrono_update_time__entry_Default();
	}
	
	/* 'default' enter sequence for region update chrono */
	private void enterSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_default() {
		react_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono__entry_Default();
	}
	
	/* 'default' enter sequence for region watch */
	private void enterSequence_main_region_Alarm_Clock_watch_default() {
		react_main_region_Alarm_Clock_watch__entry_Default();
	}
	
	/* 'default' enter sequence for region light */
	private void enterSequence_main_region_Alarm_Clock_watch_light_control_light_default() {
		react_main_region_Alarm_Clock_watch_light_control_light__entry_Default();
	}
	
	/* 'default' enter sequence for region time */
	private void enterSequence_main_region_Alarm_Clock_watch_light_control_time_default() {
		react_main_region_Alarm_Clock_watch_light_control_time__entry_Default();
	}
	
	/* 'default' enter sequence for region hide show */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_hide_show_default() {
		react_main_region_Alarm_Clock_watch_edit_hide_show__entry_Default();
	}
	
	/* 'default' enter sequence for region increase */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_increase_default() {
		react_main_region_Alarm_Clock_watch_edit_increase__entry_Default();
	}
	
	/* 'default' enter sequence for region next */
	private void enterSequence_main_region_Alarm_Clock_watch_edit_next_default() {
		react_main_region_Alarm_Clock_watch_edit_next__entry_Default();
	}
	
	/* 'default' enter sequence for region chrono */
	private void enterSequence_main_region_Alarm_Clock_chrono_default() {
		react_main_region_Alarm_Clock_chrono__entry_Default();
	}
	
	/* 'default' enter sequence for region chrono */
	private void enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_default() {
		react_main_region_Alarm_Clock_chrono_chrono_control_chrono__entry_Default();
	}
	
	/* deep enterSequence with history in child chrono */
	private void deepEnterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono() {
		switch (historyVector[0]) {
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono:
			enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono_default();
			break;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono:
			enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono_default();
			break;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono:
			enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono_default();
			break;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono:
			enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono_default();
			break;
		default:
			break;
		}
	}
	
	/* 'default' enter sequence for region alarm */
	private void enterSequence_main_region_Alarm_Clock_alarm_default() {
		react_main_region_Alarm_Clock_alarm__entry_Default();
	}
	
	/* 'default' enter sequence for region activate alarm */
	private void enterSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_default() {
		react_main_region_Alarm_Clock_alarm_alarm_activate_alarm__entry_Default();
	}
	
	/* 'default' enter sequence for region deactivate alarm */
	private void enterSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_default() {
		react_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm__entry_Default();
	}
	
	/* Default exit sequence for state tick time */
	private void exitSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time() {
		nextStateIndex = 0;
		stateVector[0] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time();
	}
	
	/* Default exit sequence for state tick chrono */
	private void exitSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono() {
		nextStateIndex = 1;
		stateVector[1] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono();
	}
	
	/* Default exit sequence for state light control */
	private void exitSequence_main_region_Alarm_Clock_watch_light_control() {
		exitSequence_main_region_Alarm_Clock_watch_light_control_light();
		exitSequence_main_region_Alarm_Clock_watch_light_control_time();
	}
	
	/* Default exit sequence for state light on */
	private void exitSequence_main_region_Alarm_Clock_watch_light_control_light_light_on() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
	}
	
	/* Default exit sequence for state delay */
	private void exitSequence_main_region_Alarm_Clock_watch_light_control_light_delay() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_watch_light_control_light_delay();
	}
	
	/* Default exit sequence for state light off */
	private void exitSequence_main_region_Alarm_Clock_watch_light_control_light_light_off() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
	}
	
	/* Default exit sequence for state update time */
	private void exitSequence_main_region_Alarm_Clock_watch_light_control_time_update_time() {
		nextStateIndex = 3;
		stateVector[3] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_watch_light_control_time_update_time();
	}
	
	/* Default exit sequence for state digitalwatch */
	private void exitSequence_main_region_Alarm_Clock_watch_digitalwatch() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_watch_digitalwatch();
	}
	
	/* Default exit sequence for state edit */
	private void exitSequence_main_region_Alarm_Clock_watch_edit() {
		exitSequence_main_region_Alarm_Clock_watch_edit_hide_show();
		exitSequence_main_region_Alarm_Clock_watch_edit_increase();
		exitSequence_main_region_Alarm_Clock_watch_edit_next();
		exitAction_main_region_Alarm_Clock_watch_edit();
	}
	
	/* Default exit sequence for state hide */
	private void exitSequence_main_region_Alarm_Clock_watch_edit_hide_show_hide() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_watch_edit_hide_show_hide();
	}
	
	/* Default exit sequence for state show */
	private void exitSequence_main_region_Alarm_Clock_watch_edit_hide_show_show() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_watch_edit_hide_show_show();
	}
	
	/* Default exit sequence for state enter */
	private void exitSequence_main_region_Alarm_Clock_watch_edit_increase_enter() {
		nextStateIndex = 3;
		stateVector[3] = State.$NullState$;
	}
	
	/* Default exit sequence for state increase selection */
	private void exitSequence_main_region_Alarm_Clock_watch_edit_increase_increase_selection() {
		nextStateIndex = 3;
		stateVector[3] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_watch_edit_increase_increase_selection();
	}
	
	/* Default exit sequence for state released */
	private void exitSequence_main_region_Alarm_Clock_watch_edit_increase_released() {
		nextStateIndex = 3;
		stateVector[3] = State.$NullState$;
	}
	
	/* Default exit sequence for state next selection */
	private void exitSequence_main_region_Alarm_Clock_watch_edit_next_next_selection() {
		nextStateIndex = 4;
		stateVector[4] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_watch_edit_next_next_selection();
	}
	
	/* Default exit sequence for state enter */
	private void exitSequence_main_region_Alarm_Clock_watch_edit_next_enter() {
		nextStateIndex = 4;
		stateVector[4] = State.$NullState$;
	}
	
	/* Default exit sequence for state held */
	private void exitSequence_main_region_Alarm_Clock_watch_edit_next_held() {
		nextStateIndex = 4;
		stateVector[4] = State.$NullState$;
	}
	
	/* Default exit sequence for state held botton right */
	private void exitSequence_main_region_Alarm_Clock_watch_held_botton_right() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_watch_held_botton_right();
	}
	
	/* Default exit sequence for state edit time */
	private void exitSequence_main_region_Alarm_Clock_watch_edit_time() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
	}
	
	/* Default exit sequence for state held botton left */
	private void exitSequence_main_region_Alarm_Clock_watch_held_botton_left() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_watch_held_botton_left();
	}
	
	/* Default exit sequence for state edit alam */
	private void exitSequence_main_region_Alarm_Clock_watch_edit_alam() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
	}
	
	/* Default exit sequence for state alam on */
	private void exitSequence_main_region_Alarm_Clock_watch_alam_on() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
	}
	
	/* Default exit sequence for state wait */
	private void exitSequence_main_region_Alarm_Clock_watch_wait() {
		nextStateIndex = 2;
		stateVector[2] = State.$NullState$;
	}
	
	/* Default exit sequence for state chrono control */
	private void exitSequence_main_region_Alarm_Clock_chrono_chrono_control() {
		exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono();
		exitAction_main_region_Alarm_Clock_chrono_chrono_control();
	}
	
	/* Default exit sequence for state display chrono */
	private void exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono() {
		nextStateIndex = 5;
		stateVector[5] = State.$NullState$;
	}
	
	/* Default exit sequence for state reset chrono */
	private void exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono() {
		nextStateIndex = 5;
		stateVector[5] = State.$NullState$;
	}
	
	/* Default exit sequence for state pause chrono */
	private void exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono() {
		nextStateIndex = 5;
		stateVector[5] = State.$NullState$;
	}
	
	/* Default exit sequence for state start chrono */
	private void exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono() {
		nextStateIndex = 5;
		stateVector[5] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono();
	}
	
	/* Default exit sequence for state wait */
	private void exitSequence_main_region_Alarm_Clock_chrono_wait() {
		nextStateIndex = 5;
		stateVector[5] = State.$NullState$;
	}
	
	/* Default exit sequence for state alarm */
	private void exitSequence_main_region_Alarm_Clock_alarm_alarm() {
		exitSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm();
		exitSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm();
		exitAction_main_region_Alarm_Clock_alarm_alarm();
	}
	
	/* Default exit sequence for state light off */
	private void exitSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off() {
		nextStateIndex = 6;
		stateVector[6] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off();
	}
	
	/* Default exit sequence for state light on */
	private void exitSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on() {
		nextStateIndex = 6;
		stateVector[6] = State.$NullState$;
		
		exitAction_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on();
	}
	
	/* Default exit sequence for state user interrupt */
	private void exitSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt() {
		nextStateIndex = 7;
		stateVector[7] = State.$NullState$;
	}
	
	/* Default exit sequence for state light off */
	private void exitSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off() {
		nextStateIndex = 7;
		stateVector[7] = State.$NullState$;
	}
	
	/* Default exit sequence for state wait */
	private void exitSequence_main_region_Alarm_Clock_alarm_wait() {
		nextStateIndex = 6;
		stateVector[6] = State.$NullState$;
	}
	
	/* Default exit sequence for region main region */
	private void exitSequence_main_region() {
		switch (stateVector[0]) {
		case main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time:
			exitSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time();
			break;
		default:
			break;
		}
		
		switch (stateVector[1]) {
		case main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono:
			exitSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono();
			break;
		default:
			break;
		}
		
		switch (stateVector[2]) {
		case main_region_Alarm_Clock_watch_light_control_light_light_on:
			exitSequence_main_region_Alarm_Clock_watch_light_control_light_light_on();
			break;
		case main_region_Alarm_Clock_watch_light_control_light_delay:
			exitSequence_main_region_Alarm_Clock_watch_light_control_light_delay();
			break;
		case main_region_Alarm_Clock_watch_light_control_light_light_off:
			exitSequence_main_region_Alarm_Clock_watch_light_control_light_light_off();
			break;
		case main_region_Alarm_Clock_watch_digitalwatch:
			exitSequence_main_region_Alarm_Clock_watch_digitalwatch();
			break;
		case main_region_Alarm_Clock_watch_edit_hide_show_hide:
			exitSequence_main_region_Alarm_Clock_watch_edit_hide_show_hide();
			break;
		case main_region_Alarm_Clock_watch_edit_hide_show_show:
			exitSequence_main_region_Alarm_Clock_watch_edit_hide_show_show();
			break;
		case main_region_Alarm_Clock_watch_held_botton_right:
			exitSequence_main_region_Alarm_Clock_watch_held_botton_right();
			break;
		case main_region_Alarm_Clock_watch_edit_time:
			exitSequence_main_region_Alarm_Clock_watch_edit_time();
			break;
		case main_region_Alarm_Clock_watch_held_botton_left:
			exitSequence_main_region_Alarm_Clock_watch_held_botton_left();
			break;
		case main_region_Alarm_Clock_watch_edit_alam:
			exitSequence_main_region_Alarm_Clock_watch_edit_alam();
			break;
		case main_region_Alarm_Clock_watch_alam_on:
			exitSequence_main_region_Alarm_Clock_watch_alam_on();
			break;
		case main_region_Alarm_Clock_watch_wait:
			exitSequence_main_region_Alarm_Clock_watch_wait();
			break;
		default:
			break;
		}
		
		switch (stateVector[3]) {
		case main_region_Alarm_Clock_watch_light_control_time_update_time:
			exitSequence_main_region_Alarm_Clock_watch_light_control_time_update_time();
			break;
		case main_region_Alarm_Clock_watch_edit_increase_enter:
			exitSequence_main_region_Alarm_Clock_watch_edit_increase_enter();
			break;
		case main_region_Alarm_Clock_watch_edit_increase_increase_selection:
			exitSequence_main_region_Alarm_Clock_watch_edit_increase_increase_selection();
			break;
		case main_region_Alarm_Clock_watch_edit_increase_released:
			exitSequence_main_region_Alarm_Clock_watch_edit_increase_released();
			break;
		default:
			break;
		}
		
		switch (stateVector[4]) {
		case main_region_Alarm_Clock_watch_edit_next_next_selection:
			exitSequence_main_region_Alarm_Clock_watch_edit_next_next_selection();
			exitAction_main_region_Alarm_Clock_watch_edit();
			break;
		case main_region_Alarm_Clock_watch_edit_next_enter:
			exitSequence_main_region_Alarm_Clock_watch_edit_next_enter();
			exitAction_main_region_Alarm_Clock_watch_edit();
			break;
		case main_region_Alarm_Clock_watch_edit_next_held:
			exitSequence_main_region_Alarm_Clock_watch_edit_next_held();
			exitAction_main_region_Alarm_Clock_watch_edit();
			break;
		default:
			break;
		}
		
		switch (stateVector[5]) {
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono:
			exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono();
			exitAction_main_region_Alarm_Clock_chrono_chrono_control();
			break;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono:
			exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono();
			exitAction_main_region_Alarm_Clock_chrono_chrono_control();
			break;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono:
			exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono();
			exitAction_main_region_Alarm_Clock_chrono_chrono_control();
			break;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono:
			exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono();
			exitAction_main_region_Alarm_Clock_chrono_chrono_control();
			break;
		case main_region_Alarm_Clock_chrono_wait:
			exitSequence_main_region_Alarm_Clock_chrono_wait();
			break;
		default:
			break;
		}
		
		switch (stateVector[6]) {
		case main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off:
			exitSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off();
			break;
		case main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on:
			exitSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on();
			break;
		case main_region_Alarm_Clock_alarm_wait:
			exitSequence_main_region_Alarm_Clock_alarm_wait();
			break;
		default:
			break;
		}
		
		switch (stateVector[7]) {
		case main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt:
			exitSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt();
			exitAction_main_region_Alarm_Clock_alarm_alarm();
			break;
		case main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off:
			exitSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off();
			exitAction_main_region_Alarm_Clock_alarm_alarm();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region update */
	private void exitSequence_main_region_Alarm_Clock_update() {
		switch (stateVector[0]) {
		case main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time:
			exitSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time();
			break;
		default:
			break;
		}
		
		switch (stateVector[1]) {
		case main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono:
			exitSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region update time */
	private void exitSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_time() {
		switch (stateVector[0]) {
		case main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time:
			exitSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region update chrono */
	private void exitSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono() {
		switch (stateVector[1]) {
		case main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono:
			exitSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region watch */
	private void exitSequence_main_region_Alarm_Clock_watch() {
		switch (stateVector[2]) {
		case main_region_Alarm_Clock_watch_light_control_light_light_on:
			exitSequence_main_region_Alarm_Clock_watch_light_control_light_light_on();
			break;
		case main_region_Alarm_Clock_watch_light_control_light_delay:
			exitSequence_main_region_Alarm_Clock_watch_light_control_light_delay();
			break;
		case main_region_Alarm_Clock_watch_light_control_light_light_off:
			exitSequence_main_region_Alarm_Clock_watch_light_control_light_light_off();
			break;
		case main_region_Alarm_Clock_watch_digitalwatch:
			exitSequence_main_region_Alarm_Clock_watch_digitalwatch();
			break;
		case main_region_Alarm_Clock_watch_edit_hide_show_hide:
			exitSequence_main_region_Alarm_Clock_watch_edit_hide_show_hide();
			break;
		case main_region_Alarm_Clock_watch_edit_hide_show_show:
			exitSequence_main_region_Alarm_Clock_watch_edit_hide_show_show();
			break;
		case main_region_Alarm_Clock_watch_held_botton_right:
			exitSequence_main_region_Alarm_Clock_watch_held_botton_right();
			break;
		case main_region_Alarm_Clock_watch_edit_time:
			exitSequence_main_region_Alarm_Clock_watch_edit_time();
			break;
		case main_region_Alarm_Clock_watch_held_botton_left:
			exitSequence_main_region_Alarm_Clock_watch_held_botton_left();
			break;
		case main_region_Alarm_Clock_watch_edit_alam:
			exitSequence_main_region_Alarm_Clock_watch_edit_alam();
			break;
		case main_region_Alarm_Clock_watch_alam_on:
			exitSequence_main_region_Alarm_Clock_watch_alam_on();
			break;
		case main_region_Alarm_Clock_watch_wait:
			exitSequence_main_region_Alarm_Clock_watch_wait();
			break;
		default:
			break;
		}
		
		switch (stateVector[3]) {
		case main_region_Alarm_Clock_watch_light_control_time_update_time:
			exitSequence_main_region_Alarm_Clock_watch_light_control_time_update_time();
			break;
		case main_region_Alarm_Clock_watch_edit_increase_enter:
			exitSequence_main_region_Alarm_Clock_watch_edit_increase_enter();
			break;
		case main_region_Alarm_Clock_watch_edit_increase_increase_selection:
			exitSequence_main_region_Alarm_Clock_watch_edit_increase_increase_selection();
			break;
		case main_region_Alarm_Clock_watch_edit_increase_released:
			exitSequence_main_region_Alarm_Clock_watch_edit_increase_released();
			break;
		default:
			break;
		}
		
		switch (stateVector[4]) {
		case main_region_Alarm_Clock_watch_edit_next_next_selection:
			exitSequence_main_region_Alarm_Clock_watch_edit_next_next_selection();
			exitAction_main_region_Alarm_Clock_watch_edit();
			break;
		case main_region_Alarm_Clock_watch_edit_next_enter:
			exitSequence_main_region_Alarm_Clock_watch_edit_next_enter();
			exitAction_main_region_Alarm_Clock_watch_edit();
			break;
		case main_region_Alarm_Clock_watch_edit_next_held:
			exitSequence_main_region_Alarm_Clock_watch_edit_next_held();
			exitAction_main_region_Alarm_Clock_watch_edit();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region light */
	private void exitSequence_main_region_Alarm_Clock_watch_light_control_light() {
		switch (stateVector[2]) {
		case main_region_Alarm_Clock_watch_light_control_light_light_on:
			exitSequence_main_region_Alarm_Clock_watch_light_control_light_light_on();
			break;
		case main_region_Alarm_Clock_watch_light_control_light_delay:
			exitSequence_main_region_Alarm_Clock_watch_light_control_light_delay();
			break;
		case main_region_Alarm_Clock_watch_light_control_light_light_off:
			exitSequence_main_region_Alarm_Clock_watch_light_control_light_light_off();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region time */
	private void exitSequence_main_region_Alarm_Clock_watch_light_control_time() {
		switch (stateVector[3]) {
		case main_region_Alarm_Clock_watch_light_control_time_update_time:
			exitSequence_main_region_Alarm_Clock_watch_light_control_time_update_time();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region hide show */
	private void exitSequence_main_region_Alarm_Clock_watch_edit_hide_show() {
		switch (stateVector[2]) {
		case main_region_Alarm_Clock_watch_edit_hide_show_hide:
			exitSequence_main_region_Alarm_Clock_watch_edit_hide_show_hide();
			break;
		case main_region_Alarm_Clock_watch_edit_hide_show_show:
			exitSequence_main_region_Alarm_Clock_watch_edit_hide_show_show();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region increase */
	private void exitSequence_main_region_Alarm_Clock_watch_edit_increase() {
		switch (stateVector[3]) {
		case main_region_Alarm_Clock_watch_edit_increase_enter:
			exitSequence_main_region_Alarm_Clock_watch_edit_increase_enter();
			break;
		case main_region_Alarm_Clock_watch_edit_increase_increase_selection:
			exitSequence_main_region_Alarm_Clock_watch_edit_increase_increase_selection();
			break;
		case main_region_Alarm_Clock_watch_edit_increase_released:
			exitSequence_main_region_Alarm_Clock_watch_edit_increase_released();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region next */
	private void exitSequence_main_region_Alarm_Clock_watch_edit_next() {
		switch (stateVector[4]) {
		case main_region_Alarm_Clock_watch_edit_next_next_selection:
			exitSequence_main_region_Alarm_Clock_watch_edit_next_next_selection();
			break;
		case main_region_Alarm_Clock_watch_edit_next_enter:
			exitSequence_main_region_Alarm_Clock_watch_edit_next_enter();
			break;
		case main_region_Alarm_Clock_watch_edit_next_held:
			exitSequence_main_region_Alarm_Clock_watch_edit_next_held();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region chrono */
	private void exitSequence_main_region_Alarm_Clock_chrono() {
		switch (stateVector[5]) {
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono:
			exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono();
			exitAction_main_region_Alarm_Clock_chrono_chrono_control();
			break;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono:
			exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono();
			exitAction_main_region_Alarm_Clock_chrono_chrono_control();
			break;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono:
			exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono();
			exitAction_main_region_Alarm_Clock_chrono_chrono_control();
			break;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono:
			exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono();
			exitAction_main_region_Alarm_Clock_chrono_chrono_control();
			break;
		case main_region_Alarm_Clock_chrono_wait:
			exitSequence_main_region_Alarm_Clock_chrono_wait();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region chrono */
	private void exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono() {
		switch (stateVector[5]) {
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono:
			exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono();
			break;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono:
			exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono();
			break;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono:
			exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono();
			break;
		case main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono:
			exitSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region alarm */
	private void exitSequence_main_region_Alarm_Clock_alarm() {
		switch (stateVector[6]) {
		case main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off:
			exitSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off();
			break;
		case main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on:
			exitSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on();
			break;
		case main_region_Alarm_Clock_alarm_wait:
			exitSequence_main_region_Alarm_Clock_alarm_wait();
			break;
		default:
			break;
		}
		
		switch (stateVector[7]) {
		case main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt:
			exitSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt();
			exitAction_main_region_Alarm_Clock_alarm_alarm();
			break;
		case main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off:
			exitSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off();
			exitAction_main_region_Alarm_Clock_alarm_alarm();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region activate alarm */
	private void exitSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm() {
		switch (stateVector[6]) {
		case main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off:
			exitSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off();
			break;
		case main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on:
			exitSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on();
			break;
		default:
			break;
		}
	}
	
	/* Default exit sequence for region deactivate alarm */
	private void exitSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm() {
		switch (stateVector[7]) {
		case main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt:
			exitSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt();
			break;
		case main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off:
			exitSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off();
			break;
		default:
			break;
		}
	}
	
	/* The reactions of state tick time. */
	private void react_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time() {
		if (check_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time_tr0_tr0()) {
			effect_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time_tr0();
		}
	}
	
	/* The reactions of state tick chrono. */
	private void react_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono() {
		if (check_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono_tr0_tr0()) {
			effect_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono_tr0();
		}
	}
	
	/* The reactions of state light on. */
	private void react_main_region_Alarm_Clock_watch_light_control_light_light_on() {
		if (check_main_region_Alarm_Clock_watch_light_control_light_light_on_tr0_tr0()) {
			effect_main_region_Alarm_Clock_watch_light_control_light_light_on_tr0();
		}
	}
	
	/* The reactions of state delay. */
	private void react_main_region_Alarm_Clock_watch_light_control_light_delay() {
		if (check_main_region_Alarm_Clock_watch_light_control_light_delay_tr0_tr0()) {
			effect_main_region_Alarm_Clock_watch_light_control_light_delay_tr0();
		} else {
			if (check_main_region_Alarm_Clock_watch_light_control_light_delay_tr1_tr1()) {
				effect_main_region_Alarm_Clock_watch_light_control_light_delay_tr1();
			}
		}
	}
	
	/* The reactions of state light off. */
	private void react_main_region_Alarm_Clock_watch_light_control_light_light_off() {
		effect_main_region_Alarm_Clock_watch_light_control_light_light_off_tr0();
	}
	
	/* The reactions of state update time. */
	private void react_main_region_Alarm_Clock_watch_light_control_time_update_time() {
		if (check_main_region_Alarm_Clock_watch_light_control_time_update_time_tr0_tr0()) {
			effect_main_region_Alarm_Clock_watch_light_control_time_update_time_tr0();
		}
	}
	
	/* The reactions of state digitalwatch. */
	private void react_main_region_Alarm_Clock_watch_digitalwatch() {
		if (check_main_region_Alarm_Clock_watch_digitalwatch_tr0_tr0()) {
			effect_main_region_Alarm_Clock_watch_digitalwatch_tr0();
		} else {
			if (check_main_region_Alarm_Clock_watch_digitalwatch_tr1_tr1()) {
				effect_main_region_Alarm_Clock_watch_digitalwatch_tr1();
			} else {
				if (check_main_region_Alarm_Clock_watch_digitalwatch_tr2_tr2()) {
					effect_main_region_Alarm_Clock_watch_digitalwatch_tr2();
				} else {
					if (check_main_region_Alarm_Clock_watch_digitalwatch_tr3_tr3()) {
						effect_main_region_Alarm_Clock_watch_digitalwatch_tr3();
					} else {
						if (check_main_region_Alarm_Clock_watch_digitalwatch_tr4_tr4()) {
							effect_main_region_Alarm_Clock_watch_digitalwatch_tr4();
						}
					}
				}
			}
		}
	}
	
	/* The reactions of state hide. */
	private void react_main_region_Alarm_Clock_watch_edit_hide_show_hide() {
		if (check_main_region_Alarm_Clock_watch_edit_hide_show_hide_tr0_tr0()) {
			effect_main_region_Alarm_Clock_watch_edit_hide_show_hide_tr0();
		} else {
			if (check_main_region_Alarm_Clock_watch_edit_hide_show_hide_tr1_tr1()) {
				effect_main_region_Alarm_Clock_watch_edit_hide_show_hide_tr1();
			}
		}
	}
	
	/* The reactions of state show. */
	private void react_main_region_Alarm_Clock_watch_edit_hide_show_show() {
		if (check_main_region_Alarm_Clock_watch_edit_hide_show_show_tr0_tr0()) {
			effect_main_region_Alarm_Clock_watch_edit_hide_show_show_tr0();
		}
	}
	
	/* The reactions of state enter. */
	private void react_main_region_Alarm_Clock_watch_edit_increase_enter() {
		if (check_main_region_Alarm_Clock_watch_edit_increase_enter_tr0_tr0()) {
			effect_main_region_Alarm_Clock_watch_edit_increase_enter_tr0();
		}
	}
	
	/* The reactions of state increase selection. */
	private void react_main_region_Alarm_Clock_watch_edit_increase_increase_selection() {
		if (check_main_region_Alarm_Clock_watch_edit_increase_increase_selection_tr0_tr0()) {
			effect_main_region_Alarm_Clock_watch_edit_increase_increase_selection_tr0();
		} else {
			if (check_main_region_Alarm_Clock_watch_edit_increase_increase_selection_tr1_tr1()) {
				effect_main_region_Alarm_Clock_watch_edit_increase_increase_selection_tr1();
			}
		}
	}
	
	/* The reactions of state released. */
	private void react_main_region_Alarm_Clock_watch_edit_increase_released() {
		effect_main_region_Alarm_Clock_watch_edit_increase_released_tr0();
	}
	
	/* The reactions of state next selection. */
	private void react_main_region_Alarm_Clock_watch_edit_next_next_selection() {
		if (check_main_region_Alarm_Clock_watch_edit_next_next_selection_tr0_tr0()) {
			effect_main_region_Alarm_Clock_watch_edit_next_next_selection_tr0();
		} else {
			if (check_main_region_Alarm_Clock_watch_edit_next_next_selection_tr1_tr1()) {
				effect_main_region_Alarm_Clock_watch_edit_next_next_selection_tr1();
			}
		}
	}
	
	/* The reactions of state enter. */
	private void react_main_region_Alarm_Clock_watch_edit_next_enter() {
		if (check_main_region_Alarm_Clock_watch_edit_next_enter_tr0_tr0()) {
			effect_main_region_Alarm_Clock_watch_edit_next_enter_tr0();
		}
	}
	
	/* The reactions of state held. */
	private void react_main_region_Alarm_Clock_watch_edit_next_held() {
		effect_main_region_Alarm_Clock_watch_edit_next_held_tr0();
	}
	
	/* The reactions of state held botton right. */
	private void react_main_region_Alarm_Clock_watch_held_botton_right() {
		if (check_main_region_Alarm_Clock_watch_held_botton_right_tr0_tr0()) {
			effect_main_region_Alarm_Clock_watch_held_botton_right_tr0();
		} else {
			if (check_main_region_Alarm_Clock_watch_held_botton_right_tr1_tr1()) {
				effect_main_region_Alarm_Clock_watch_held_botton_right_tr1();
			}
		}
	}
	
	/* The reactions of state edit time. */
	private void react_main_region_Alarm_Clock_watch_edit_time() {
		effect_main_region_Alarm_Clock_watch_edit_time_tr0();
	}
	
	/* The reactions of state held botton left. */
	private void react_main_region_Alarm_Clock_watch_held_botton_left() {
		if (check_main_region_Alarm_Clock_watch_held_botton_left_tr0_tr0()) {
			effect_main_region_Alarm_Clock_watch_held_botton_left_tr0();
		} else {
			if (check_main_region_Alarm_Clock_watch_held_botton_left_tr1_tr1()) {
				effect_main_region_Alarm_Clock_watch_held_botton_left_tr1();
			}
		}
	}
	
	/* The reactions of state edit alam. */
	private void react_main_region_Alarm_Clock_watch_edit_alam() {
		effect_main_region_Alarm_Clock_watch_edit_alam_tr0();
	}
	
	/* The reactions of state alam on. */
	private void react_main_region_Alarm_Clock_watch_alam_on() {
		effect_main_region_Alarm_Clock_watch_alam_on_tr0();
	}
	
	/* The reactions of state wait. */
	private void react_main_region_Alarm_Clock_watch_wait() {
		if (check_main_region_Alarm_Clock_watch_wait_tr0_tr0()) {
			effect_main_region_Alarm_Clock_watch_wait_tr0();
		}
	}
	
	/* The reactions of state display chrono. */
	private void react_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono() {
		if (check_main_region_Alarm_Clock_chrono_chrono_control_tr0_tr0()) {
			effect_main_region_Alarm_Clock_chrono_chrono_control_tr0();
		} else {
			if (check_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono_tr0_tr0()) {
				effect_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono_tr0();
			}
		}
	}
	
	/* The reactions of state reset chrono. */
	private void react_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono() {
		if (check_main_region_Alarm_Clock_chrono_chrono_control_tr0_tr0()) {
			effect_main_region_Alarm_Clock_chrono_chrono_control_tr0();
		} else {
			effect_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono_tr0();
		}
	}
	
	/* The reactions of state pause chrono. */
	private void react_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono() {
		if (check_main_region_Alarm_Clock_chrono_chrono_control_tr0_tr0()) {
			effect_main_region_Alarm_Clock_chrono_chrono_control_tr0();
		} else {
			if (check_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono_tr0_tr0()) {
				effect_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono_tr0();
			} else {
				if (check_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono_tr1_tr1()) {
					effect_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono_tr1();
				}
			}
		}
	}
	
	/* The reactions of state start chrono. */
	private void react_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono() {
		if (check_main_region_Alarm_Clock_chrono_chrono_control_tr0_tr0()) {
			effect_main_region_Alarm_Clock_chrono_chrono_control_tr0();
		} else {
			if (check_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono_tr0_tr0()) {
				effect_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono_tr0();
			} else {
				if (check_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono_tr1_tr1()) {
					effect_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono_tr1();
				}
			}
		}
	}
	
	/* The reactions of state wait. */
	private void react_main_region_Alarm_Clock_chrono_wait() {
		if (check_main_region_Alarm_Clock_chrono_wait_tr0_tr0()) {
			effect_main_region_Alarm_Clock_chrono_wait_tr0();
		}
	}
	
	/* The reactions of state light off. */
	private void react_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off() {
		if (check_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off_tr0_tr0()) {
			effect_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off_tr0();
		} else {
			if (check_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off_tr1_tr1()) {
				effect_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off_tr1();
			}
		}
	}
	
	/* The reactions of state light on. */
	private void react_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on() {
		if (check_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on_tr0_tr0()) {
			effect_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on_tr0();
		}
	}
	
	/* The reactions of state user interrupt. */
	private void react_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt() {
		if (check_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt_tr0_tr0()) {
			effect_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt_tr0();
		}
	}
	
	/* The reactions of state light off. */
	private void react_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off() {
		effect_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off_tr0();
	}
	
	/* The reactions of state wait. */
	private void react_main_region_Alarm_Clock_alarm_wait() {
		if (check_main_region_Alarm_Clock_alarm_wait_tr0_tr0()) {
			effect_main_region_Alarm_Clock_alarm_wait_tr0();
		}
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region_Alarm_Clock_update_update_time_and_chrono_update_time__entry_Default() {
		enterSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono__entry_Default() {
		enterSequence_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region_Alarm_Clock_update__entry_Default() {
		enterSequence_main_region_Alarm_Clock_update_update_time_and_chrono_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region_Alarm_Clock_watch_light_control_light__entry_Default() {
		enterSequence_main_region_Alarm_Clock_watch_light_control_light_light_on_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region_Alarm_Clock_watch_light_control_time__entry_Default() {
		enterSequence_main_region_Alarm_Clock_watch_light_control_time_update_time_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region_Alarm_Clock_watch_edit_hide_show__entry_Default() {
		enterSequence_main_region_Alarm_Clock_watch_edit_hide_show_hide_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region_Alarm_Clock_watch_edit_increase__entry_Default() {
		enterSequence_main_region_Alarm_Clock_watch_edit_increase_enter_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region_Alarm_Clock_watch_edit_next__entry_Default() {
		enterSequence_main_region_Alarm_Clock_watch_edit_next_enter_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region_Alarm_Clock_watch__entry_Default() {
		enterSequence_main_region_Alarm_Clock_watch_wait_default();
	}
	
	/* Default react sequence for deep history entry  */
	private void react_main_region_Alarm_Clock_chrono_chrono_control_chrono__entry_Default() {
		/* Enter the region with deep history */
		if (historyVector[0] != State.$NullState$) {
			deepEnterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono();
		} else {
			enterSequence_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono_default();
		}
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region_Alarm_Clock_chrono__entry_Default() {
		enterSequence_main_region_Alarm_Clock_chrono_wait_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region_Alarm_Clock_alarm__entry_Default() {
		enterSequence_main_region_Alarm_Clock_alarm_wait_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region_Alarm_Clock_alarm_alarm_activate_alarm__entry_Default() {
		enterSequence_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm__entry_Default() {
		enterSequence_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt_default();
	}
	
	/* Default react sequence for initial entry  */
	private void react_main_region__entry_Default() {
		enterSequence_main_region_Alarm_Clock_default();
	}
	
	/* The reactions of exit default. */
	private void react_main_region_Alarm_Clock_watch_light_control_light__exit_Default() {
		effect_main_region_Alarm_Clock_watch_light_control_tr0();
	}
	
	/* The reactions of exit default. */
	private void react_main_region_Alarm_Clock_watch_edit_hide_show__exit_Default() {
		effect_main_region_Alarm_Clock_watch_edit_tr0();
	}
	
	/* The reactions of exit default. */
	private void react_main_region_Alarm_Clock_watch_edit_next__exit_Default() {
		effect_main_region_Alarm_Clock_watch_edit_tr0();
	}
	
	/* The reactions of exit default. */
	private void react_main_region_Alarm_Clock_alarm_alarm_activate_alarm__exit_Default() {
		effect_main_region_Alarm_Clock_alarm_alarm_tr0();
	}
	
	/* The reactions of exit default. */
	private void react_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm__exit_Default() {
		effect_main_region_Alarm_Clock_alarm_alarm_tr0();
	}
	
	public void runCycle() {
		if (!initialized)
			throw new IllegalStateException(
					"The state machine needs to be initialized first by calling the init() function.");
		clearOutEvents();
		for (nextStateIndex = 0; nextStateIndex < stateVector.length; nextStateIndex++) {
			switch (stateVector[nextStateIndex]) {
			case main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time:
				react_main_region_Alarm_Clock_update_update_time_and_chrono_update_time_tick_time();
				break;
			case main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono:
				react_main_region_Alarm_Clock_update_update_time_and_chrono_update_chrono_tick_chrono();
				break;
			case main_region_Alarm_Clock_watch_light_control_light_light_on:
				react_main_region_Alarm_Clock_watch_light_control_light_light_on();
				break;
			case main_region_Alarm_Clock_watch_light_control_light_delay:
				react_main_region_Alarm_Clock_watch_light_control_light_delay();
				break;
			case main_region_Alarm_Clock_watch_light_control_light_light_off:
				react_main_region_Alarm_Clock_watch_light_control_light_light_off();
				break;
			case main_region_Alarm_Clock_watch_light_control_time_update_time:
				react_main_region_Alarm_Clock_watch_light_control_time_update_time();
				break;
			case main_region_Alarm_Clock_watch_digitalwatch:
				react_main_region_Alarm_Clock_watch_digitalwatch();
				break;
			case main_region_Alarm_Clock_watch_edit_hide_show_hide:
				react_main_region_Alarm_Clock_watch_edit_hide_show_hide();
				break;
			case main_region_Alarm_Clock_watch_edit_hide_show_show:
				react_main_region_Alarm_Clock_watch_edit_hide_show_show();
				break;
			case main_region_Alarm_Clock_watch_edit_increase_enter:
				react_main_region_Alarm_Clock_watch_edit_increase_enter();
				break;
			case main_region_Alarm_Clock_watch_edit_increase_increase_selection:
				react_main_region_Alarm_Clock_watch_edit_increase_increase_selection();
				break;
			case main_region_Alarm_Clock_watch_edit_increase_released:
				react_main_region_Alarm_Clock_watch_edit_increase_released();
				break;
			case main_region_Alarm_Clock_watch_edit_next_next_selection:
				react_main_region_Alarm_Clock_watch_edit_next_next_selection();
				break;
			case main_region_Alarm_Clock_watch_edit_next_enter:
				react_main_region_Alarm_Clock_watch_edit_next_enter();
				break;
			case main_region_Alarm_Clock_watch_edit_next_held:
				react_main_region_Alarm_Clock_watch_edit_next_held();
				break;
			case main_region_Alarm_Clock_watch_held_botton_right:
				react_main_region_Alarm_Clock_watch_held_botton_right();
				break;
			case main_region_Alarm_Clock_watch_edit_time:
				react_main_region_Alarm_Clock_watch_edit_time();
				break;
			case main_region_Alarm_Clock_watch_held_botton_left:
				react_main_region_Alarm_Clock_watch_held_botton_left();
				break;
			case main_region_Alarm_Clock_watch_edit_alam:
				react_main_region_Alarm_Clock_watch_edit_alam();
				break;
			case main_region_Alarm_Clock_watch_alam_on:
				react_main_region_Alarm_Clock_watch_alam_on();
				break;
			case main_region_Alarm_Clock_watch_wait:
				react_main_region_Alarm_Clock_watch_wait();
				break;
			case main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono:
				react_main_region_Alarm_Clock_chrono_chrono_control_chrono_display_chrono();
				break;
			case main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono:
				react_main_region_Alarm_Clock_chrono_chrono_control_chrono_reset_chrono();
				break;
			case main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono:
				react_main_region_Alarm_Clock_chrono_chrono_control_chrono_pause_chrono();
				break;
			case main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono:
				react_main_region_Alarm_Clock_chrono_chrono_control_chrono_start_chrono();
				break;
			case main_region_Alarm_Clock_chrono_wait:
				react_main_region_Alarm_Clock_chrono_wait();
				break;
			case main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off:
				react_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_off();
				break;
			case main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on:
				react_main_region_Alarm_Clock_alarm_alarm_activate_alarm_light_on();
				break;
			case main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt:
				react_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_user_interrupt();
				break;
			case main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off:
				react_main_region_Alarm_Clock_alarm_alarm_deactivate_alarm_light_off();
				break;
			case main_region_Alarm_Clock_alarm_wait:
				react_main_region_Alarm_Clock_alarm_wait();
				break;
			default:
				// $NullState$
			}
		}
		clearEvents();
	}
}
